package com.leonelle.spaceships;

import java.util.*;

/**
 * Created by pformela on 2017-01-22.
 */
public class App {
    final static int preexistingShips = 10;
    final static int preexistingStations = 2;

    public static void main(String[] args) {
        Position3D spawnPoint;
        Random generator = new Random();

        LinkedList<Transporter> cargoShips = new LinkedList<>();

        System.out.println("Creating transporters...");
        for (int ID = 0; ID < preexistingShips; ID++) {
            spawnPoint = new Position3D(generator.nextInt(500), generator.nextInt(500), generator.nextInt(500));
            Transporter cargo = new Transporter(ID);
            cargo.setPosition(spawnPoint);
            cargoShips.add(cargo);
            System.out.println(cargoShips.getLast() + " in position " + cargoShips.getLast().getPosition());
        }

        LinkedList<SpaceStation> stations = new LinkedList<>();

        System.out.println("\nCreating Base Stations...");
        for (int ID = 0; ID < preexistingStations; ID++) {
            spawnPoint = new Position3D(generator.nextInt(1000), generator.nextInt(1000), generator.nextInt(1000));
            SpaceStation homeBase = new SpaceStation(ID);
            homeBase.setPosition(spawnPoint);
            stations.add(homeBase);
            System.out.println(homeBase + " in position " + homeBase.getPosition());
        }

        // Linking the ships to their home bases

        TreeMap<Transporter, Integer> origin = new TreeMap<>();

        for (Transporter ship : cargoShips) {
            origin.put(ship, generator.nextInt(2));
        }

        Set relations = origin.entrySet();
        Iterator i = relations.iterator();

        System.out.println("\nShips and their home bases:");
        while (i.hasNext()) {
            Map.Entry currentShip = (Map.Entry) i.next();
            System.out.println(currentShip.getKey() + " -> " + stations.get((int) currentShip.getValue()));
        }

    }
}
