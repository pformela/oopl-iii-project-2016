package com.leonelle.spaceships;

public abstract class SpaceObject {
    protected int ID;
    protected String name;
    protected Position3D position;

    public SpaceObject() {
        this.name = "UnnamedObject";
        this.position = new Position3D();
    }

    public SpaceObject(int ID, String name) {
        this.ID = ID;
        this.name = name;
        this.position = new Position3D();
    }

    public SpaceObject(int ID) {
        this.ID = ID;
        this.name = "SpaceObject " + ID;
        this.position = new Position3D();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Position3D getPosition() {
        return position;
    }

    public void setPosition(Position3D position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " + this.getID() + ": \"" + this.name + "\"";
    }

    public boolean equals(SpaceObject so) {
        if (this.getPosition() == so.getPosition()) return true;
        else return false;
    }
}
