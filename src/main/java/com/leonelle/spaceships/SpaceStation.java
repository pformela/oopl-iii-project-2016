package com.leonelle.spaceships;

public class SpaceStation extends SpaceObject {
    int stationFuel = 127980;

    public SpaceStation() {
        super();
        this.name = "UnnamedStation";
    }

    public SpaceStation(int ID, String name) {
        super(ID, name);
    }

    public SpaceStation(int ID) {
        super(ID);
        this.name = "Space Station " + ID;
    }

    public int getStationFuel() {
        return stationFuel;
    }

    public void setStationFuel(int stationFuel) {
        this.stationFuel = stationFuel;
    }
}
