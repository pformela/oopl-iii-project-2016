package com.leonelle.spaceships;

/**
 *
 */
public interface Chargeable {
    void addFuel(int amount) throws OutOfBoundsException;

    void useFuel(int amount) throws CannotMoveException;

    int getFuel();

    void setFuel(int amount) throws OutOfBoundsException;
}
