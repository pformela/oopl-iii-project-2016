package com.leonelle.spaceships;

/**
 * Created by pformela on 2017-01-22.
 */
public interface Embarkable {
    int getPassengers();

    void setPassengers(int passengers) throws OutOfBoundsException;

    void addPassengers(int passengers) throws OutOfBoundsException;

    void remPassengers(int passengers) throws OutOfBoundsException;
}
