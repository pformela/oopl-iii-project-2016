package com.leonelle.spaceships;

public class Position3D {
    private int x, y, z;

    public Position3D() {
        this.x = this.y = this.z = 0;
    }

    public Position3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    void flyTo(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    void fly(int x, int y, int z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    int[] getPosition() {
        int[] curPosition = new int[]{this.x, this.y, this.z};
        return curPosition;
    }

    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + "," + this.z + ")";
    }

    public boolean equals(Position3D p) {
        if (this.x == p.x && this.y == p.y && this.z == p.z) return true;
        else return false;
    }
}
