package com.leonelle.spaceships;

/**
 * Created by pformela on 2017-01-22.
 */
public class CannotMoveException extends Exception {
    public CannotMoveException() {
        super("This won't move anymore!");
    }
}
