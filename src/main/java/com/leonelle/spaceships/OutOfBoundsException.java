package com.leonelle.spaceships;

/**
 * Created by pformela on 2017-01-22.
 */
public class OutOfBoundsException extends Exception {
    public OutOfBoundsException(int lowBound, int upBound) {
        super("The value needs to fit between " + lowBound + " and " + upBound + "!");
    }
}
