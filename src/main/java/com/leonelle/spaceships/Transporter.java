package com.leonelle.spaceships;

public class Transporter extends Spaceship implements Embarkable {
    protected int passengers;

    public Transporter(int ID) {
        super(ID);
        this.passengers = 0;
        this.name = "Transporter " + ID;
    }

    public Transporter(int ID, String name) {
        super(ID, name);
        this.passengers = 0;
    }

    public Transporter(int ID, int passengers) {
        super(ID);
        this.passengers = passengers;
        this.name = "Transporter " + ID;
    }

    public Transporter(int ID, String name, int passengers) {
        super(ID, name);
        this.passengers = passengers;
    }

    @Override
    public int getPassengers() {
        return this.passengers;
    }

    @Override
    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    @Override
    public void addPassengers(int passengers) {
        this.passengers += passengers;
    }

    @Override
    public void remPassengers(int passengers) {
        this.passengers -= passengers;
    }
}
