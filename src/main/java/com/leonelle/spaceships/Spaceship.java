package com.leonelle.spaceships;

import java.util.Comparator;

public class Spaceship extends SpaceObject implements Chargeable, Comparator<Spaceship>, Comparable<Spaceship> {
    protected int FuelLeft = 100;

    public Spaceship(int ID, String name) {
        super(ID, name);
    }

    public Spaceship(int ID) {
        super(ID);
        this.name = "Generic Spaceship " + ID;
    }

    @Override
    public void addFuel(int amount) throws OutOfBoundsException {
        if (FuelLeft + amount <= 100 || amount < 0)
            this.FuelLeft += amount;
        else throw new OutOfBoundsException(0, 100 - this.FuelLeft);
    }

    @Override
    public void useFuel(int amount) throws CannotMoveException {
        if (this.FuelLeft <= 0 || amount < 0)
            throw new CannotMoveException();
        else this.FuelLeft -= amount;
    }

    @Override
    public int getFuel() {
        return FuelLeft;
    }

    @Override
    public void setFuel(int amount) throws OutOfBoundsException {
        if (amount > 100 || amount < 0)
            throw new OutOfBoundsException(0, 100);
        else FuelLeft = amount;
    }

    public int compareTo(Spaceship s) {
        return ((Integer) this.ID).compareTo(s.ID);
    }

    public int compare(Spaceship s1, Spaceship s2) {
        return s1.ID - s2.ID;
    }
}
