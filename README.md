# Object-Oriented Programming Languages III 2016/17 final project #
Author: Przmysław Formela

## Summary.
This is a simple spaceship trade sorta-simulator (it doesn't simulate anything, really).
* There are cargo ships.
* There are space stations.
* Cargo ships belong to space stations.
That's it.

## Compilation and running
* "mvn package" creates an executable .jar file.
* console of your choice -> "java -jar App-1.0-SNAPSHOT.jar"
    * alternatively you may just build and run the project in your IDE.